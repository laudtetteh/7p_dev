<?php get_header(); ?>

<article class="pg<?php echo sp_pg_classes(); ?> page">
    <?php if ( get_the_title() ) : ?>
    <header class="pg-hd page-hd">
        <h1 class="pg-title page-title"><?php print get_the_title(); ?></h1>
    </header>
    <?php endif; ?>

    <div class="pg-body page-body">
    <?php while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
        <?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'discover' ), 'after' => '' ) ); ?>
        <?php comments_template( '', true ); ?>
    <?php endwhile; // end of the loop. ?>
    </div>
</article>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
