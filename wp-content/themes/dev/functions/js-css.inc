<?php
/**
 * @sp_default_register_styles
 * ----------------------------------------------------------------------------
 * Add js/css to header
 */
function sp_default_add_styles() {
    wp_register_style( 'html5bp-normalize', get_template_directory_uri() . '/css/html5bp/normalize.css');
    wp_register_style( 'html5bp-main', get_template_directory_uri() . '/css/html5bp/main.css');
    // wp_register_style( 'master', get_template_directory_uri() . '/css/master.css');
    // wp_register_style( 'master', 'https://www.dropbox.com/s/.css?dl=1');

    wp_enqueue_style('html5bp-normalize');
    wp_enqueue_style('html5bp-main');
    wp_enqueue_style('master');
}

add_action( 'wp_enqueue_scripts', 'sp_default_add_styles' );

/**
 * @sp_default_jquery_in_footer
 * ----------------------------------------------------------------------------
 * Moves jquery to the footer.
 */

function sp_default_jquery_in_footer( &$scripts ) {
    if ( ! is_admin() )
        $scripts->add_data( 'jquery', 'group', 1 );
}

add_action( 'wp_default_scripts', 'sp_default_jquery_in_footer' );

/**
 * @sp_default_add_scripts
 * ----------------------------------------------------------------------------
 * Add scripts
 */
function sp_default_add_scripts() {
    // add modernizr in header
    wp_enqueue_script(
        'modernizr',
        get_template_directory_uri() . '/js/vendor/modernizr-2.6.2.min.js'
    );

    // add jquery in footer
    wp_deregister_script( 'jquery' );
    wp_register_script(
        'jquery',
        'http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js',
        '',
        '',
        true
    );
    wp_enqueue_script('jquery');

    // add jquery UI Effectsjquery-ui-autocomplete
    // wp_enqueue_script( 'jquery-effects-core' );
    // wp_enqueue_script( 'jquery-effects-highlight' );

    // add html5bp plugins.js
    wp_enqueue_script(
        'html5bp-plugins',
        get_template_directory_uri() . '/js/html5bp/plugins.js',
        array( 'jquery' ),
        false,
        true // load in footer
    );

    // add Chosen form styler: http://harvesthq.github.io/chosen/
    wp_register_style( 'chosen-css', get_template_directory_uri() . '/js/chosen/chosen.min.css');
    wp_enqueue_style( 'chosen-css' );
    wp_enqueue_script(
        'chosen-js',
        get_template_directory_uri() . '/js/chosen/chosen.jquery.min.js',
        array( 'jquery' ),
        '0.12.1',
        true // load in footer
    );

    // add custom.js
    wp_enqueue_script(
        'stf-default-custom',
        get_template_directory_uri() . '/js/custom.js',
        array( 'jquery' ),
        false,
        true  // load in footer
    );
}

add_action( 'wp_enqueue_scripts', 'sp_default_add_scripts' );

?>
