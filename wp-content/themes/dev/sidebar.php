<?php
$primary_menu_settings = array(
    'menu'            => 2,
    'container'       => 'nav',
    'menu_class'      => 'menu',
    'echo'            => false,
    'level'           => 2,
    'depth'           => 2
);

$primary_menu = wp_nav_menu( $primary_menu_settings );
$sidebar_fields = get_field( 'sidebar' );
?>

<aside class="l-sidebar">
<?php if ( !empty( $primary_menu ) ) : // show menu if one exists ?>
    <div class="sidebar">
        <h4>Menu</h4>
        <?php echo $primary_menu; ?>
    </div>
<?php endif; ?>

<?php if ( !empty( $sidebar_fields ) ) : // show sidebar field if content exists ?>
    <div class="sidebar sidebar-field">
        <?php the_field('sidebar'); ?>
    </div>
<?php endif; ?>

<?php if ( sp_sidebar_visibility() == 'on' ) : // show global widget if exists ?>
    <div class="sidebar widget-area">
        <?php dynamic_sidebar('primary_widget_area'); ?>
    </div>
<?php endif; ?>
</aside>
