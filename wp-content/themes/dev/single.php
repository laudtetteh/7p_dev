<?php get_header(); ?>

<article class="pg<?php echo sp_pg_classes(); ?> <?php echo get_post_type( $post ); ?>">
  <header class="pg-hd <?php echo get_post_type( $post ); ?>-hd">
    <h1 class="pg-title <?php echo get_post_type( $post ); ?>-title"><?php the_title(); ?></h1>
  </header>

  <div class="pg-body <?php echo get_post_type( $post ); ?>-body">
    <?php while ( have_posts() ) : the_post(); ?>

      <?php the_content(); ?>
      <?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'discover' ), 'after' => '' ) ); ?>
      <?php comments_template( '', true ); ?>

    <?php endwhile; // end of the loop. ?>
  </div>
</article>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
