<?php 
/**
 * INSTRUCTIONS
 * ----------------------------------------------------------------------------
 * @.footer-push (optional)
 *      Use this to make footer stick to bottom of page.
 *      Requires additional css. See sass/layout/_footer.sass
 *      Taken from: http://ryanfait.com/resources/footer-stick-to-bottom-of-page
 * 
 * @dynamic_sidebar() (optional)
 *      Setup a widget to place content in these locations if desired. 
 *      http://codex.wordpress.org/Function_Reference/dynamic_sidebar
 * 
 * 1.) Google Analytics
 *      Update and delete this code.
 *      <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
 */
?>
        <?php if (is_home() == FALSE) :  // only push footer down for subpages ?>
            <div class="footer-push"></div>
        <?php endif; ?>
        </div>
    </div>
    <footer class="l-footer">
        <div class="clearfix footer-pt1">
            <?php dynamic_sidebar('footer_pt1'); ?>
        </div>
        <div class="clearfix footer-pt2">
            <?php dynamic_sidebar('footer_pt2'); ?>
        </div>
        <div class="clear credits">
            <div class="credits-pt1">
                &copy; <?php print date("Y"); ?> <a href="http://www.example.com" target="_blank">Example Corp</a> All Rights Reserved
            </div>
            <div class="credits-pt2">
                <nav class="credits-nav">
                    <ul class="credits-nav-items">
                        <li class="credits-nav-item"><a href="/" class="credits-nav-link" title=""></a></li>
                        <li class="credits-nav-item"><a href="/" class="credits-nav-link" title=""></a></li>
                        <li class="credits-nav-item"><a href="/" class="credits-nav-link" title=""></a></li>
                        <li class="credits-nav-item"><a href="/" class="credits-nav-link" title=""></a></li>
                        <li class="credits-nav-item"><a href="/" class="credits-nav-link" title=""></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </footer>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        // var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
        // (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
        // g.src='//www.google-analytics.com/ga.js';
        // s.parentNode.insertBefore(g,s)}(document,'script'));
    </script>
    <?php wp_footer(); ?>
</body>
</html>