<?php 
/**
 * INSTRUCTIONS
 * ----------------------------------------------------------------------------
 * 1.) If you have Google Fonts to add, place <link> tag directly in <head>
 * 
 * @.page-wrap
 *      Only necessary if site design requires a wrapper. 
 *      Otherwise may be removed.
*/
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo get_bloginfo( 'name' ); ?><?php wp_title( '|', true, 'left' ); ?></title>
    <meta name="viewport" content="width=device-width">

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <div class="page-wrap">
        <a name="top"></a>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <header class="l-site-hd site-hd">
            <div class="site-hd-wrap">
                <a href="/" class="logo" title=""><img src="<?php print get_template_directory_uri(); ?>/images/logo.png" width="230" height="100" /></a>
                <div class="site-hd-search"></div>
                <nav class="site-hd-nav">
                    <ul class="site-hd-nav-items">
                        <li class="site-hd-nav-item"><a href="/" class="site-hd-nav-link" title=""></a></li>
                        <li class="site-hd-nav-item"><a href="/" class="site-hd-nav-link" title=""></a></li>
                        <li class="site-hd-nav-item"><a href="/" class="site-hd-nav-link" title=""></a></li>
                        <li class="site-hd-nav-item"><a href="/" class="site-hd-nav-link" title=""></a></li>
                        <li class="site-hd-nav-item"><a href="/" class="site-hd-nav-link" title=""></a></li>
                    </ul>
                </nav>
            </div>
        </header>
        <div class="clearfix l-main">