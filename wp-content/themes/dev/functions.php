<?php
/**
 * IMAGES
 * ----------------------------------------------------------------------------
 */

/**
 * add thumbnail support for News, Events, and Video
 * http://codex.wordpress.org/Function_Reference/add_theme_support
 */
add_theme_support( 'post-thumbnails', array( 'post' ) );

/**
 * add custom image size
 * http://codex.wordpress.org/Function_Reference/add_image_size
 */

// Uncomment to add custom image style.
// if ( function_exists( 'add_image_size' ) ) {
//     add_image_size( 'My Custom Image Size', 270, 160 , true);
// }

/**
 * JAVASCRIPT & CSS
 * ----------------------------------------------------------------------------
 */

/**
 * @js-css.inc
 * Add, reorder, js & css files.
 */
include_once 'functions/js-css.inc';

/**
 * SIDEBAR & WIDGET CONTENT
 * ----------------------------------------------------------------------------
 */

/**
 * @sp_default_widgets_init()
 * Registers sidebars in the widget area.
 *
 * Footer Part One/Two:
 *      Name/Description can be changed to make more sense to the client.
 *      But keep ids the same.
 */
function sp_default_widgets_init() {
    // Add a Main Sidebar that can be used by pages and all post types.
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'sp_default' ),
        'id' => 'main_sidebar',
        'description' => __( 'Main sidebar.', 'sp_default' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ) );

    // Adds a sidebar for the footer area.
    // Set this way so client can edit footer content.

    register_sidebar( array(
        'name' => __( 'Footer Part One', 'sp_default' ),
        'id' => 'footer_pt1',
        'description' => __( 'Content for the first footer area.', 'sp_default' ),
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ) );

    // Register the second footer area.
    register_sidebar( array(
        'name' => __( 'Footer Part Two', 'sp_default' ),
        'id' => 'footer_pt2',
        'description' => __( 'Content for the second footer area.', 'sp_default' ),
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ) );
}

add_action( 'widgets_init', 'sp_default_widgets_init' );

/**
 * @sp_sidebar_visibility
 * Returns on/off value and tests for sidebar visibility.
 * Add conditions here to turn sidebar on/off for different pages.
 */
function sp_sidebar_visibility() {
    $menu_id = 2; // main menu menu_id.
    $sidebar_fields = get_field( 'sidebar' ); // Check for content in sidebar field
    $primary_menu = wp_get_nav_menu_items( $menu_id ); // Check if current page is in main menu
    // $query = $_SERVER['REQUEST_URI'];
    // $current_url = explode('/', $query );

    // If there is no custom content in sidebar field and
    // there is no item for this post in the main menu
    if ( ( empty( $sidebar_fields ) ) && ( empty( $primary_menu ) ) ) {
        $sidebar_visibility = 'off';
    }

    // default to visible
    else {
        $sidebar_visibility = 'on';
    }

    return $sidebar_visibility;
}

/**
 * @sp_pg_classes
 * Conditional classes that are applied in page.php, single.php
 * Default just adds a full width class if sidebar is turned off.
 */
function sp_pg_classes() {
    // Get the sidebar visibility.
    $sidebar_visibility = sp_sidebar_visibility();

    // Add full width class if sidebar is turned off.
    $class = $sidebar_visibility == 'off' ? ' l-full-width' : '';

    return $class;
}

/**
 * @sp_get_partial()
 * Used instead of get_template_part() so variables can be
 * passed to partial include via an array.
 */
function sp_get_partial ( $file, $args = array() ) {
    $dir = get_stylesheet_directory();
    $partials_dir = 'partials/';
    $filepath = $dir . '/' . $partials_dir . $file . '.php';

    if ( is_array( $args ) )
        extract( $args );

    return include( $filepath );
}
