<?php get_header(); ?>

<article class="pg<?php echo sp_pg_classes(); ?> news-post">
  <header class="pg-hd news-post-hd">
    <h1 class="pg-title news-post-title"><?php the_title(); ?></h1>
  </header>

  <div class="pg-body news-post-body">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <?php
        $feature_image_id = get_post_thumbnail_id( $post_id );
        $feature_image = get_post( $feature_image_id );
        $caption = $feature_image->post_excerpt;
        ?>

        <?php if ( has_post_thumbnail() ) : ?>
        <div class="feature-img news-post-img">
            <?php the_post_thumbnail('medium'); ?>
            <?php if ( !empty( $caption ) ) : ?>
                <p class="feature-img news-post-img-cap"><?php echo $caption; ?></p>
            <?php endif; ?>
        </div>
        <?php endif; ?>

        <h3 class="pg-date news-post-date"><?php the_date(); ?></h3>
        <?php the_content(); ?>
        <?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'discover' ), 'after' => '' ) ); ?>
        <?php comments_template( '', true ); ?>

    <?php endwhile; endif; // end of the loop. ?>
  </div>
</article>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
