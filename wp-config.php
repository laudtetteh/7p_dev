<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '7p_dev_wp');

/** MySQL database username */
define('DB_USER', 'laudtetteh');

/** MySQL database password */
define('DB_PASSWORD', 'Why82Why1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7gkkANROLivZL0s5rMm6baXUuKU58RLCXhmGK4DQGtuLiTPYZzluUy0bodm0syLq');
define('SECURE_AUTH_KEY',  'NSfS8NbbHhNJAHGHOgLjB6UeMqN6j7pOR838ESleyQuMbTqbLkhlFRmU6fxYpq4f');
define('LOGGED_IN_KEY',    'SBEtpmebMG6v3g34VG3uXlrdCKO40oy5IqzNhh6vIkCTNLfrSjyvKt0lW3XGmneV');
define('NONCE_KEY',        'sx3btP3erKXRUDBDCfuojR16GbPPiuz8tAOazc5qycDiMPY6mqecbT2Ul0U8NdSM');
define('AUTH_SALT',        'GPhdMurqGYjU2cDZw9hyo36oyfqJhPAQ4AK8NMvaulA9UFGqsgrKdNr4SoGJ70co');
define('SECURE_AUTH_SALT', 'ZlQi77fMEGOaGEH9l6tFW8mBzBqhD6FzvuQLIZP1uhvCMqDYWd7UXt49aBJqSM5g');
define('LOGGED_IN_SALT',   'xgnSwuoQHdCl3CHbWntg6fnuVIiDczLCOmj01R50UGHNFkxo9n97wOj28pnWgYGE');
define('NONCE_SALT',       'wqYn6BErTZJzw4QQqU39mcktn6C3N0QtOA5z5XSGnnDCSo0DWmI8E0mEDG1FukEq');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define( 'WP_ALLOW_MULTISITE', true );
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/7pixels/dev/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
